#include <iostream>
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <chrono>
#include <bits/stdc++.h>

using namespace std;

int port = 13345;
int rankProc = 0;//Номер процесса
int sizeProc = 0;//Количество процессов

sockaddr_in soketSend;//Сокет для отправки сообщений
sockaddr_in soketRecv;//Сокет для приёма сообщений
int socket_Send;
int socket_Recv;

const double e = 10e-5; //Порог сходимости
int size_message = 5000000;

int i_len = 0;
int j_len = 0;

//Вспомогательные функции
int MPI_Init_();//Насройка сокетов
void MPI_Reduce_(double *max_i, double *max, int rank, int action);//Реализация MPI_Reduce на сокетах 
void MPI_Send_( double *send_, int len, int rank);//Реализация MPI_Send на сокетах
void MPI_Recv_( double *recv_, int len, int rank);//Реализация MPI_Recv на сокетах
void tokenize(std::string const &str, const char delim,std::vector<std::string> &out);//Разделение строки

int main(int argc, char *argv[])
{
    //Инициализация данных
    rankProc = atoi(argv[1]);
    sizeProc = atoi(argv[2]);
    int status = MPI_Init_();
    if (status == -1){
        return 0;
    }

    //Считываем матрицу коэффициентов А и вектор свобоных членов В из файлов
    string line;
    vector<vector<std::string>> out;//матрицу коэффициентов А
    vector<vector<std::string>> out1;//вектор свобоных членов В

    //А-------------------------------------------------->
    ifstream in("a.txt"); // окрываем файл для чтения
    if (in.is_open())
    {
        while (getline(in, line))
        {
            vector<std::string> out_i;
            const char delim = ' ';
            tokenize(line, delim, out_i);
            out.push_back(out_i);
        }
    }
    in.close();// закрываем файл

    //Получаем размерность
    i_len = out.size();
    j_len = out.at(0).size();

    double **a = new double *[i_len];
    for (int i = 0; i < i_len; i++){
        a[i] = new double[j_len];
    }    

    for (int i = 0; i < i_len; i++){
        for (int j = 0; j < j_len; j++){
            a[i][j] = stod(out.at(i).at(j));
        }
    }    

    //B-------------------------------------------------->
    ifstream in1("b.txt");
    if (in1.is_open())
    {
        while (getline(in1, line))
        {
            vector<std::string> out_i;
            const char delim = ' ';
            tokenize(line, delim, out_i);
            out1.push_back(out_i);
        }
    }
    in1.close();// закрываем файл    

    double *b = new double [j_len];

    for (int j = 0; j < j_len; j++){
        b[j] = stod(out1.at(0).at(j));
    }

    //Получим количесвтво строк для каждого процесса
    i_len = i_len/sizeProc;

    //Преобразуем А и В и запишем начальное приближение
    double **a_val = new double *[i_len];//Матрица А
    double *b_val = new double [i_len];//Вектор свободных членов В
    double *x_val = new double [j_len];//Текущее приближение
    double *x_val_pred = new double [j_len];//Предыдущее приближение
    for (int i = 0; i < i_len; i++){
        a_val[i] = new double[j_len];
    }
    //А
    for (int i = 0; i < i_len; i++){
        for (int j = 0; j < j_len; j++){
            if (i+rankProc*i_len == j){
                a_val[i][j] = 0.0;
            } else {
                a_val[i][j] = -1*(a[i+rankProc*i_len][j]/a[i+rankProc*i_len][i+rankProc*i_len]);
            }
        }
    }
    //В
    for (int i = 0; i < i_len; i++){
        b_val[i] = b[i+rankProc*i_len]/a[i+rankProc*i_len][i+rankProc*i_len];
    }

    //Начального приближения x0
    for (int i = 0; i < j_len; i++){
        x_val[i] = 1;
    }

    //cout << "i here ->" << rankProc << endl;

    double max = 1;//МАкисимальная разница между текущим и предыдущим приближением по x
    int iter = 1;//Номер итерации
    clock_t start, end;
    start = clock();
    while (true){
        //Запоминаем предыдущие значение
        for (int i = 0; i < j_len; i++){
            x_val_pred[i] = x_val[i];
        }

        //Обнуляем вектор x
        for (int i = 0; i < j_len; i++){
            x_val[i] = 0.0;
        }

        //Высчитываем новое приближение
        for (int i = 0; i < i_len; i++){

            for (int j = 0; j < j_len; j++){
                x_val[i+rankProc*i_len] = x_val[i+rankProc*i_len] + x_val_pred[j]*a_val[i][j];
            }

            x_val[i+rankProc*i_len] = x_val[i+rankProc*i_len] + b_val[i];

        }

        //Проверяем условие
        double max_i = 0.0;
        for (int i = 0; i < i_len; i++){
            if (fabs(x_val[i+rankProc*i_len] - x_val_pred[i+rankProc*i_len]) > max_i){
                max_i = fabs(x_val[i+rankProc*i_len] - x_val_pred[i+rankProc*i_len]);
            }
        }

        //cout << "i here 1 ->" << rankProc << endl;

        //Выполняем MPI_Reduce
        if (sizeProc != 0){
            MPI_Reduce_(&max_i, &max, 0, 0);
        }

        //cout << "i here 2 ->" << rankProc << endl;


        //Если основной процесс отправляем другим процессам max
        if (rankProc == 0){
            for (int i = 0; i < sizeProc; i++){
                if (i != rankProc){
                    double *send = new double [1];
                    send[0] = max;
                    MPI_Send_(send, 1, i);
                }
            }
        }  

        //cout << "i here 3 ->" << rankProc << endl;


        //Если не основной процесс ролучаем от основного max
        if (rankProc != 0){
            double *rec = new double [1];
            MPI_Recv_(rec, 1, 0);
            max = rec[0];
        }

        //Проверяем условие 
        if (max < e){
            break;
        }

        //cout << "i here 4 ->" << rankProc << endl;


        //Производим обмен значениями вектора x между процессами
        //Если основной процесс
        if (rankProc == 0){
            //Получаем все значения
            for (int i = 0; i < sizeProc; i++){
                if (i != rankProc){
                    //Получаем значения от других процессов
                    double *rec = new double [i_len];
                    MPI_Recv_(rec, i_len, i);

                    for (int j = 0; j < i_len; j++){
                        x_val[j+i*i_len] = rec[j];
                    }
                }
            }

            //Отправляем все значения
            for (int i = 0; i < sizeProc; i++){
                if (i != rankProc){
                    MPI_Send_(x_val, j_len, i);
                }
            }
        }

        //Если не основной процесс
        if (rankProc != 0){
            //Отправляем своё значение
            double *send = new double [i_len];
            for (int j = 0; j < i_len; j++){
                send[j] = x_val[j+rankProc*i_len];
            }
            MPI_Send_(send, i_len, 0);

            //Получаем все значения
            MPI_Recv_(x_val, j_len, 0);
        }


        //cout << "i here 5 ->" << rankProc << endl;

        iter++;
    }
    end = clock();
    double time_taken = double(end - start) / double(CLOCKS_PER_SEC);

    if (rankProc == 0){
        cout << "Time: " << fixed << time_taken << setprecision(5) << endl;
    }

    /*for (int i = 0; i < i_len; i++){
        cout << "<------x" << i+rankProc*i_len+1 << "=" << x_val[i+rankProc*i_len] << endl;
    }*/

    close(socket_Recv);
    close(socket_Send);

    delete[] a_val;
    delete[] a;
    delete[] x_val;
    delete[] b_val;
    delete[] b;

    return 0;
}

//Насройка сокетов
int MPI_Init_(){
    //Настраиваем сокет для приёма сообщений
    bzero((char*)&soketRecv, sizeof(soketRecv));
    soketRecv.sin_family = AF_INET;
    soketRecv.sin_addr.s_addr = htonl(INADDR_ANY);
    soketRecv.sin_port = htons(port + rankProc);

    //Создаём сокет
    socket_Recv = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_Recv < 0){
        cout << rankProc << "->Socket: Error" << endl;
        return -1;
    }

    int bind_ = bind(socket_Recv, (struct sockaddr*) &soketRecv, sizeof(soketRecv));
    if(bind_ < 0){
        cout << rankProc << "->Bind: Error" << endl;
        return -1;
    }

    //Настраиваем сокет для отправки сообщений
    bzero((char*)&soketSend, sizeof(soketSend));
    soketSend.sin_family = AF_INET;
    soketSend.sin_addr.s_addr = htonl(INADDR_ANY);

    socket_Send = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_Send < 0){
        cout << rankProc << "->Socket: Error" << endl;
        return -1;
    }

    return 1;
}
void MPI_Reduce_(double *max_i, double *max, int rank, int action){
    //Если основной процесс
    if (rankProc == rank){
        listen(socket_Recv, 15);
        int col = 0;
        int accept_;
        char msg[size_message];
        while (col < sizeProc-1){
            sockaddr_in newSockAddr;
            socklen_t newSockAddrSize = sizeof(newSockAddr);

            accept_ = accept(socket_Recv, (sockaddr *)&newSockAddr, &newSockAddrSize);

            recv(accept_, (char*)msg, sizeof(msg), 0);

            double num_double = atof(msg);


            if (num_double > (*max_i)){
                (*max_i) = num_double;
            }

            col = col + 1;
            close(accept_);
        }

        (*max) = (*max_i);
    }

    if (rankProc != rank){
        int status = -1; 
        char msg[size_message];
        sprintf (msg, "%f", (*max_i));

        soketSend.sin_port = htons(port + rank);
        socket_Send = socket(AF_INET, SOCK_STREAM, 0);
        if(socket_Send < 0){
            cout << rankProc << "->Socket: Error" << endl;
            return;
        }
        while (status < 0 ){
            status = connect(socket_Send, (sockaddr*) &soketSend, sizeof(soketSend));   
        }
        send(socket_Send, (char*)msg, strlen(msg), 0);
        close(socket_Send);
    }
}
void MPI_Send_( double *send_, int len, int rank){
    soketSend.sin_port = htons(port + rank);
    socket_Send = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_Send < 0){
        cout << rankProc << "->Socket: Error" << endl;
        return;
    }
    int status = -1; 
    while (status < 0 ){
        status = connect(socket_Send, (sockaddr*) &soketSend, sizeof(soketSend));   
    }

    char combine[size_message];
    string ss = "";
    for (int i = 0; i < len; i++){
        ss = ss + to_string(send_[i]);
        if (i < len - 1){
            ss = ss + " ";
        }
    }

    ss=ss+"!";
    
    for (int i = 0; i < ss.length(); i++){
        combine[i] = ss.at(i);
    }

    send(socket_Send, (char*)combine, sizeof(combine), 0);
    close(socket_Send);
}
void MPI_Recv_( double *recv_, int len, int rank){
    listen(socket_Recv, 15);

    char msg[size_message];
    sockaddr_in newSockAddr;
    socklen_t newSockAddrSize = sizeof(newSockAddr);

    int accept_ = accept(socket_Recv, (sockaddr *)&newSockAddr, &newSockAddrSize);

    recv(accept_, (char*)msg, sizeof(msg), 0);

    int i = 0;
    string nn = "";
    while(msg[i] != '!'){
        nn = nn + msg[i];
        i = i + 1;
    }

    vector<std::string> double_i;

    const char delim = ' ';
    tokenize(nn, delim, double_i);

    for (int i = 0; i < len; i++){
        (recv_[i]) = stod(double_i.at(i));
    }

    close(accept_);
}
void tokenize(std::string const &str, const char delim, std::vector<std::string> &out)
{
    // строим поток из строки
    std::stringstream ss(str);
 
    std::string s;
    while (std::getline(ss, s, delim)) {
        out.push_back(s);
    }
}