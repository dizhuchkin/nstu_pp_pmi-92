#include <iostream>
#include <stdio.h>
#include "mpi.h"
#include <cstring>
#include <string.h>
#include <cmath>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <sstream>

using namespace std;

int sizeProc = 0;//Количество процессов
int rankProc = 0;//Номер процесса

const double e = 10e-5; //Порог сходимости

int i_len = 0;
int j_len = 0;

void tokenize(std::string const &str, const char delim,
            std::vector<std::string> &out)
{
    // строим поток из строки
    std::stringstream ss(str);
 
    std::string s;
    while (std::getline(ss, s, delim)) {
        out.push_back(s);
    }
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &sizeProc); //Записываем количество процессов
    MPI_Comm_rank(MPI_COMM_WORLD, &rankProc); //Записываем номер текущего процесса

    double startwtime = 0.0, endwtime;

    string line;
    vector<vector<std::string>> out;
    vector<vector<std::string>> out1;

    //Записываем а_val
    ifstream in("a.txt"); // окрываем файл для чтения
    if (in.is_open())
    {
        while (getline(in, line))
        {
            vector<std::string> out_i;
            const char delim = ' ';
            tokenize(line, delim, out_i);
            out.push_back(out_i);
        }
    }
    in.close();     // закрываем файл

    i_len = out.size();
    j_len = out.at(0).size();

    double **a = new double *[i_len];
    for (int i = 0; i < i_len; i++){
        a[i] = new double[j_len];
    }

    //Заполняем матрицу a
    for (int i = 0; i < i_len; i++){
        for (int j = 0; j < j_len; j++){
            a[i][j] = stod(out.at(i).at(j));
        }
    }

    ifstream in1("b.txt");
    if (in1.is_open())
    {
        while (getline(in1, line))
        {
            vector<std::string> out_i;
            const char delim = ' ';
            tokenize(line, delim, out_i);
            out1.push_back(out_i);
        }
    }
    in1.close();     // закрываем файл

    double *b = new double [j_len];

    //Заполняем матрицу b
    for (int j = 0; j < j_len; j++){
        b[j] = stod(out1.at(0).at(j));
    }

    i_len = i_len/sizeProc;

    //Инициализируем матрицу коэффициентов a
    double **a_val = new double *[i_len];
    for (int i = 0; i < i_len; i++){
        a_val[i] = new double[j_len];
    }

    //Заполняем матрицу a
    for (int i = 0; i < i_len; i++){
        for (int j = 0; j < j_len; j++){
            if (i+rankProc*i_len == j){
                a_val[i][j] = 0.0;
            } else {
                a_val[i][j] = -1*(a[i+rankProc*i_len][j]/a[i+rankProc*i_len][i+rankProc*i_len]);
            }
        }
    }

    //Инициализируем вектор свободных членов b
    double *b_val = new double [i_len];
    //Заполняем вектор свободных членов b
    for (int i = 0; i < i_len; i++){
        b_val[i] = b[i+rankProc*i_len]/a[i+rankProc*i_len][i+rankProc*i_len];
    }

    //Инициализируем вектор начального приближения x0
    double *x_val = new double [j_len];
    //Заполняем вектор начального приближения x0
    for (int i = 0; i < j_len; i++){
        x_val[i] = 1;
    }

    double *x_val_pred = new double [j_len];
    double max = 1;
    int iter = 1;
    startwtime = MPI_Wtime();
    while (true){

        //Запоминаем предыдущие значение
        for (int i = 0; i < j_len; i++){
            x_val_pred[i] = x_val[i];
        }

        //Обнуляем вектор x
        for (int i = 0; i < j_len; i++){
            x_val[i] = 0.0;
        }

        //Высчитываем новое приближение
        for (int i = 0; i < i_len; i++){

            for (int j = 0; j < j_len; j++){
                x_val[i+rankProc*i_len] = x_val[i+rankProc*i_len] + x_val_pred[j]*a_val[i][j];
            }

            x_val[i+rankProc*i_len] = x_val[i+rankProc*i_len] + b_val[i];

        }

        //Проверяем условие
        double max_i = 0.0;
        for (int i = 0; i < i_len; i++){
            if (fabs(x_val[i+rankProc*i_len] - x_val_pred[i+rankProc*i_len]) > max_i){
                max_i = fabs(x_val[i+rankProc*i_len] - x_val_pred[i+rankProc*i_len]);
            }
        }

        //Сравниваем знаяения max|xk-xk-1|
        MPI_Reduce(&max_i, &max, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

        MPI_Status status;
        int tag = 0;

        //Если основной процесс отправляем другим процессам max
        if (rankProc == 0){
            for (int i = 0; i < sizeProc; i++){
                if (i != rankProc){
                    double *send = new double [1];
                    send[0] = max;
                    MPI_Send(
                        send, 1, MPI_DOUBLE, i, tag,
                        MPI_COMM_WORLD
                    );
                }
            }
        }  

        //Если не основной процесс ролучаем от основного max
        if (rankProc != 0){
            double *rec = new double [1];
            MPI_Recv(
                rec, 1, MPI_DOUBLE, 0, tag,
                MPI_COMM_WORLD, &status
            );

            max = rec[0];
        }

        if (max < e){
            break;
        }

        //Производим обмен значениями вектора x между процессами
        //Если основной процесс
        if (rankProc == 0){
            //Получаем все значения
            for (int i = 0; i < sizeProc; i++){
                if (i != rankProc){
                    //Получаем значения от других процессов
                    double *rec = new double [i_len];
                    MPI_Recv(
                        rec, i_len, MPI_DOUBLE, i, tag,
                        MPI_COMM_WORLD, &status
                    );

                    for (int j = 0; j < i_len; j++){
                        x_val[j+i*i_len] = rec[j];
                    }
                }
            }
            //Отправляем все значения
            for (int i = 0; i < sizeProc; i++){
                if (i != rankProc){
                    MPI_Send(
                        x_val, j_len, MPI_DOUBLE, i, tag,
                        MPI_COMM_WORLD
                    );
                }
            }
        }

        //Если не основной процесс
        if (rankProc != 0){
            //Отправляем своё значение
            double *send = new double [i_len];
            for (int j = 0; j < i_len; j++){
                send[j] = x_val[j+rankProc*i_len];
            }
            MPI_Send(
                send, i_len, MPI_DOUBLE, 0, tag,
                MPI_COMM_WORLD
            );

            //Получаем все значения
            MPI_Recv(
                x_val, j_len, MPI_DOUBLE, 0, tag,
                MPI_COMM_WORLD, &status
            );
        }
        iter++;
    }
    endwtime = MPI_Wtime();


    if (rankProc == 0){
        cout << "Time: " << endwtime - startwtime << endl;
    }
    
    /*for (int i = 0; i < i_len; i++){
        cout << "x" << i+rankProc*i_len+1 << "=" << x_val[i+rankProc*i_len] << endl;
    }*/

    for (int i = 0; i < i_len; i++){
        delete[] a_val[i];
        delete[] a[i];        
    }

    delete[] a_val;
    delete[] a;
    delete[] x_val;
    delete[] b_val;
    delete[] b;


    MPI_Finalize();
    return 0;
}