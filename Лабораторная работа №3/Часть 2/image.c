#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>

#define IMAGESIZE 54
#define BILLION 1000000000.0

#pragma pack(push, 2)
typedef struct
{ // Структура изображения
  char sign;
  int size;
  int notused;
  int data;
  int headwidth;
  int width;
  int height;
  short numofplanes;
  short bitpix;
  int method;
  int arraywidth;
  int horizresol;
  int vertresol;
  int colnum;
  int basecolnum;
} img;
#pragma pop

// Вспомогательные функции
char *openImage(int inputFileNumber, img *bmp);                             // Открытие изображения
void createImage(char *imgdata, img *bmp, int radius, int inputFileNumber); // Создание изображения
int border(int i, int min, int max);                                        // Установка границ

// Основная функция
int main(int argc, char *argv[])
{
  unsigned char *imgdata;
  img *bmp = (img *)malloc(IMAGESIZE);
  // RGB
  unsigned char *red;
  unsigned char *green;
  unsigned char *blue;

  int radius = atoi(argv[1]);                // Считываем радиус Гауссовского фильтра
  int inputFileNumber = atoi(argv[2]);       // Название файла с изображением
  int colParallel = atoi(argv[3]);           // Количество потоков
  imgdata = openImage(inputFileNumber, bmp); // Открываем изображение

  int width = bmp->width;
  int height = bmp->height;

  int i, j;
  int rgb_width = width * 3;
  if ((width * 3 % 4) != 0)
  {
    rgb_width += (4 - (width * 3 % 4));
  }

  red = (unsigned char *)malloc(width * height);
  green = (unsigned char *)malloc(width * height);
  blue = (unsigned char *)malloc(width * height);
  // Замер времени
  struct timespec start, end;

  // Получаем значения red, green, blue для каждого пикселя
  int pos = 0;
  for (i = 0; i < height; i++)
  {
    for (j = 0; j < width * 3; j += 3, pos++)
    {
      red[pos] = imgdata[i * rgb_width + j];
      green[pos] = imgdata[i * rgb_width + j + 1];
      blue[pos] = imgdata[i * rgb_width + j + 2];
    }
  }

  // Подсчёт времени
  clock_gettime(CLOCK_REALTIME, &start);

  omp_set_num_threads(colParallel);

// Параллельная область
#pragma omp parallel for schedule(static, 100) private(j)
  for (i = 0; i < height; i++)
  {
    for (j = 0; j < width; j++)
    {

      if ((i == 0) && (j == 0))
      {
        int id = omp_get_num_threads();
        printf("Number of threads = %d\n", id);
      }

      int row;
      int col;
      double redSum = 0;
      double greenSum = 0;
      double blueSum = 0;
      double weightSum = 0;

      // Расчёт для пикселя RGB Гаусовского фильтра
      for (row = i - radius; row <= i + radius; row++)
      {
        for (col = j - radius; col <= j + radius; col++)
        {
          int x = border(col, 0, width - 1); // Работа с краями
          int y = border(row, 0, height - 1);
          int tempPos = y * width + x;
          double square = (col - j) * (col - j) + (row - i) * (row - i);
          double sigma = radius * radius;
          double weight = exp(-square / (2 * sigma)) / (3.14 * 2 * sigma);
          redSum += red[tempPos] * weight;
          greenSum += green[tempPos] * weight;
          blueSum += blue[tempPos] * weight;
          weightSum += weight;
        }
      }
      red[i * width + j] = round(redSum / weightSum);
      green[i * width + j] = round(greenSum / weightSum);
      blue[i * width + j] = round(blueSum / weightSum);
      redSum = 0;
      greenSum = 0;
      blueSum = 0;
      weightSum = 0;
    }
  }

  clock_gettime(CLOCK_REALTIME, &end); // Конец замера времени

  double time_spent = (end.tv_sec - start.tv_sec) +
                      (end.tv_nsec - start.tv_nsec) / BILLION;

  printf("Time filter is %f seconds\n", time_spent);

  // Создаём отфильтрованное изображение
  pos = 0;
  for (i = 0; i < height; i++)
  {
    for (j = 0; j < width * 3; j += 3, pos++)
    {
      imgdata[i * rgb_width + j] = red[pos];
      imgdata[i * rgb_width + j + 1] = green[pos];
      imgdata[i * rgb_width + j + 2] = blue[pos];
    }
  }
  createImage(imgdata, bmp, radius, inputFileNumber);

  free(red);
  free(green);
  free(blue);
  free(bmp);
  return 0;
}

char *openImage(int inputFileNumber, img *in)
{
  char inPutFileNameBuffer[32];
  sprintf(inPutFileNameBuffer, "%d.bmp", inputFileNumber);

  FILE *file;
  if (!(file = fopen(inPutFileNameBuffer, "rb")))
  {
    printf("File not found!");
    free(in);
    exit(1);
  }
  fread(in, 54, 1, file);

  char *data = (char *)malloc(in->arraywidth);
  fseek(file, in->data, SEEK_SET);
  fread(data, in->arraywidth, 1, file);
  fclose(file);
  return data;
}

void createImage(char *imgdata, img *out, int radius, int inputFileNumber)
{
  FILE *file;
  time_t now;
  time(&now);
  char fileNameBuffer[32];
  sprintf(fileNameBuffer, "FILTER_%d_%d.bmp", radius, inputFileNumber);
  file = fopen(fileNameBuffer, "wb");
  fwrite(out, IMAGESIZE, 1, file);
  fseek(file, out->data, SEEK_SET);
  fwrite(imgdata, out->arraywidth, 1, file);
  fclose(file);
}

int border(int i, int min, int max)
{
  if (i < min)
    return min;
  else if (i > max)
    return max;
  return i;
}