#include <cstdlib> 
#include <cstring> 
#include <iostream> 
#include <unistd.h> 
#include <pthread.h>
#include <vector>
#include <time.h>

using namespace std; 

//Вывод ошибки
#define err_exit(code, str) { cerr << str << ": " << strerror(code) \ 
 << endl; exit(EXIT_FAILURE); } 

//Место на складе
struct place {
    bool state;
    bool use;
    int store; 
    pthread_mutex_t mutex; 
    pthread_cond_t cond; 
};
vector <place> warehouse;//склад
int number_place;//количество пустых мест


clock_t start1;
clock_t end1;
int first = 0;

void *producer(void *arg) //Функция производителя
{ 
    int err; 

    cout << "Producer start" << endl;

    while(true) { 
        if (number_place == warehouse.size()){

            if (first != 0){
                end1 = clock();
                long double seconds = (long double)(end1 - start1) / CLOCKS_PER_SEC;
                double kol = number_place/seconds;
                cout << "<------------------------------Speed:" << kol << " task/sec" << endl;
            }
            first = first + 1;

            for (int i = 0; i < warehouse.size(); i++){//проходимся по складу
                // Захватываем мьютекс и ожидаем освобождения склада
                err = pthread_mutex_lock(&warehouse.at(i).mutex); 
                if(err != 0) 
                    err_exit(err, "Cannot lock mutex");
                while(warehouse.at(i).state == true) { //Пока товар есть
                    err = pthread_cond_wait(&warehouse.at(i).cond, &warehouse.at(i).mutex); //Блокировка до момента наступления события
                    if(err != 0) 
                        err_exit(err, "Cannot wait on condition variable"); 
                }

                // Получен сигнал, что на складе не осталось товаров. 
                // Производим новый товар. 
                warehouse.at(i).store = rand();
                warehouse.at(i).state = true;
                number_place = number_place - 1;
                //cout << "Producer number " << warehouse.at(i).store << "...send" << endl;

                // Посылаем сигнал, что на складе появился товар. 
                err = pthread_cond_signal(&warehouse.at(i).cond); 
                if(err != 0) 
                    err_exit(err, "Cannot send signal"); 

                err = pthread_mutex_unlock(&warehouse.at(i).mutex); 
                if(err != 0) 
                    err_exit(err, "Cannot unlock mutex"); 
            }
            start1 = clock();
        } 
    } 
} 

void *consumer(void *arg) //Функция производителя
{ 
    int err; 
    long param = (long)arg;

    cout << "Consuming "<< param << " start " << endl;

    while(true) { // Захватываем мьютекс и ожидаем появления товаров на складе
        if ((number_place >= 0) && (number_place < warehouse.size())){
            for (int i = 0; i < warehouse.size(); i++){//проходимся по складу
                if (warehouse.at(i).use == false){
                    warehouse.at(i).use = true;
                    // Захватываем мьютекс и ожидаем освобождения склада
                    err = pthread_mutex_lock(&warehouse.at(i).mutex); 
                    if(err != 0) 
                        err_exit(err, "Cannot lock mutex");

                    while(warehouse.at(i).state == false) { //Пока товар есть
                        err = pthread_cond_wait(&warehouse.at(i).cond, &warehouse.at(i).mutex); //Блокировка до момента наступления события
                        if(err != 0) 
                        err_exit(err, "Cannot wait on condition variable"); 
                    }

                    // Получен сигнал, что на складе имеется товар. 
                    // Потребляем его. 
                    //cout << "Consuming "<< param << " number " << warehouse.at(i).store << "...take start" << endl; 
                    sleep(1); 
                    //cout << "Consuming "<< param << " number " << warehouse.at(i).store << "...take stop" << endl;
                    warehouse.at(i).state = false;
                    number_place = number_place + 1;

                    // Посылаем сигнал, что на складе появился товар. 
                    err = pthread_cond_signal(&warehouse.at(i).cond); 
                    if(err != 0) 
                        err_exit(err, "Cannot send signal"); 

                    err = pthread_mutex_unlock(&warehouse.at(i).mutex); 
                    if(err != 0) 
                        err_exit(err, "Cannot unlock mutex"); 

                    warehouse.at(i).use = false;
                }
            }
        }
    } 
} 

int main(int argc, char *argv[]) 
{ 
    int err; //Результат выполнения функций

    if (argc != 3){
        cout << "Parameters entered incorrecrly" << endl;
        return -1;
    }

    //параметры сервера
    int consumers = atoi(argv[1]); //количество потербителей
    int size_buf = atoi(argv[2]); //количество складов

    //проверка параметров
    if ((consumers <= 1) || (size_buf <= 1)){
        cout << "Parameters entered incorrecrly" << endl;
        return -1;
    }
  
    // Идентификаторы потоков
    pthread_t threadProducer; //индефикатор производителя
    vector <pthread_t> threadConsumer(consumers);
    warehouse.reserve(3);

    //инициализируем мьютексы и условные переменные
    for (int i = 0; i < size_buf; i++){

        place place1;
        place1.state = false;
        place1.use = false;
        place1.store = 0;
        err = pthread_cond_init(&place1.cond, NULL); 
        if(err != 0) 
            err_exit(err, "Cannot initialize condition variable"); 
        err = pthread_mutex_init(&place1.mutex, NULL); 
        if(err != 0) 
            err_exit(err, "Cannot initialize mutex"); 

        warehouse.push_back(place1);

    }

    number_place = size_buf;

    cout << "Конфигурация модели:" << endl;
    cout << "Количество производителей: 1" << endl;
    cout << "Количество потребителей: " << consumers << endl;
    cout << "Количество мест на складе: " << size_buf << endl;

    // Создаём потоки
    err = pthread_create(&threadProducer, NULL, producer, NULL); 
    if(err != 0) 
        err_exit(err, "Cannot create threadProducer");

    for (int i = 0; i < threadConsumer.size(); i++){
        int param = i + 1;
        err = pthread_create(&threadConsumer.at(i), NULL, consumer,  (void *)param); 
        if(err != 0) 
            err_exit(err, "Cannot create threadConsumer"); 
    }

    // Дожидаемся завершения потоков
    pthread_join(threadProducer, NULL); 

    for (int i = 0; i < threadConsumer.size(); i++){
        pthread_join(threadConsumer.at(i), NULL); 
    }

    // Освобождаем ресурсы, связанные с мьютексом
    // и условной переменной
    for (int i = 0; i < size_buf; i++){
        pthread_mutex_destroy(&warehouse.at(i).mutex); 
        pthread_cond_destroy(&warehouse.at(i).cond); 
    }
} 