#include <cstdlib>
#include <iostream>
#include <time.h>
#include <cstring>
#include <pthread.h>
#include <vector>
#include <utility>

using namespace std;

#define err_exit(code, str) { cerr << str << ": " << strerror(code) << endl; exit(EXIT_FAILURE); }
int N = 0;//указатель на номер элемента, который изменяем
pthread_mutex_t mutex;
int H[3] = {0,0,0};//Числа в диапазоне 1: от 1 до 10, 2: от 11 до 100, 3: от 101 и более
double res[3];
void *MAP(void *arg){
	int el;
	int err;
	vector<int> *p = (vector<int> *)arg;
	vector<int> &list = *p;
	while(true){
		err = pthread_mutex_lock(&mutex);
		if(err!=0)
			err_exit(err, "Cannot lock mutex");
		el = N;
		N++;
		err = pthread_mutex_unlock(&mutex);
		if(err!=0)
			err_exit(err, "Cannot unlock mutex");	
		if(el < list.size()){
			if (list[el]>0 and list[el]<=10)
				H[0]++;
			else{
				if (list[el]>10 and list[el]<=100)
					H[1]++;
				else
					H[2]++;
			}			
		}else return NULL;
			
	}
}

void *reduce(void *arg){
	int el;
	int count=0;
	int err;
	for (int i=0;i<3;i++)
		count = count + H[i];
	while(true){
		err = pthread_mutex_lock(&mutex);
		if(err!=0)
			err_exit(err, "Cannot lock mutex");
		el = N;
		N++;
		err = pthread_mutex_unlock(&mutex);
		if(err!=0)
			err_exit(err, "Cannot unlock mutex");	
		if(el < 3){
			res[el] = double(H[el])/count*100;		
		}else return NULL;
			
	}
}

void MapReduce(void *Reduce(void*), void *MaP(void*), vector<int> R, int kol){
	N=0;pthread_t thread;
	pthread_attr_t thread_attr;
	int err;
	vector<int> *m = &R;
	// Создаём индефикаторы потоков
	vector<pthread_t> threads(kol);
	
	//Запускаем потоки
	for (int i=0; i < kol; i++){
		err = pthread_create(&threads.at(i), NULL,MaP, (void *) m);
	
		if(err != 0){
			cout << "Cannot create a thread: " << strerror(err) << endl;
			exit(-1);
		}
	}
	// ожидаем окончание всех потоков
	for (int i = 0; i < threads.size(); i++)
	{
		pthread_join(threads.at(i), NULL);
	}
	
	N=0;	
	//Запускаем потоки
	for (int i=0; i < kol; i++){
		err = pthread_create(&threads.at(i), NULL,Reduce, (void *) m);
	
		if(err != 0){
			cout << "Cannot create a thread: " << strerror(err) << endl;
			exit(-1);
		}
	}
	// ожидаем окончание всех потоков
	for (int i = 0; i < threads.size(); i++)
	{
		pthread_join(threads.at(i), NULL);
	}
}
//Add 4
int func1(int d){
	return d+4;
}

//Mul 2
int func2(int d){
	return d*2;
}

//d^2
int func3(int d){
	return d*d;
}


void *thread_job(void *arg)
{
	vector<int> *p = (vector<int> *)arg;
	vector<int> &mas = *p;
	int NUM_EL;
	int err;
	while(true){
		err = pthread_mutex_lock(&mutex);
		if(err!=0)
			err_exit(err, "Cannot lock mutex");
		NUM_EL = N;
		N++;
		err = pthread_mutex_unlock(&mutex);
		if(err!=0)
			err_exit(err, "Cannot unlock mutex");	
		if(NUM_EL < mas.size()-1){
			switch(mas[mas.size()-1]){
				case(1):{
					mas[NUM_EL]=func1(mas[NUM_EL]);
					break;
				}
				case(2):{
					mas[NUM_EL]=func2(mas[NUM_EL]);
					break;
				}
				case(3):{
					mas[NUM_EL]=func3(mas[NUM_EL]);
					break;
				}
				default:{
					cout << "Error!!! Wrong number of function";
					exit(-1);
					break;
				}
			}
			
		}else return NULL;
			
	}
}

int main()
{
	pthread_t thread;
	pthread_attr_t thread_attr;
	int err;
	vector<int> *m = new vector<int>();
	vector<int> &p = *m;
	int numf, kolp,sizemas;
	cout << "Enter the number of function:";
	cin >> numf;
	cout << "Enter size of mass:";
	cin >> sizemas;
	cout << "Enter the value of threads:";
	cin >> kolp;
	
	//Заполняем массив случайными числами 1 до 20
	srand(time(NULL));
	cout << "Start:\n";
	for (int i=0; i < sizemas; i++){
		p.push_back(rand()%20+1);
		cout << p[i] << " ";
	}
		
	p.push_back(numf);
	
	cout << "\n";
	//Mutex
	err = pthread_mutex_init(&mutex, NULL);
	if (err!=0)
		err_exit(err, "Cannot initialize mutex");
		
	// Создаём индефикаторы потоков
	vector<pthread_t> threads(kolp);
	
	//Запускаем потоки
	for (int i=0; i < kolp; i++){
		err = pthread_create(&threads.at(i), NULL,thread_job, (void *) m);
	
		if(err != 0){
			cout << "Cannot create a thread: " << strerror(err) << endl;
			exit(-1);
		}
	}
	// ожидаем окончание всех потоков
	for (int i = 0; i < threads.size(); i++)
	{
		pthread_join(threads.at(i), NULL);
	}

	cout << "Rezult:\n";
	for (int i=0; i < sizemas; i++){
		cout << p[i] << " ";
	}
	cout << "\n";
	cout << "Введите количество потоков для функции MapReduce:";
	cin >> kolp;
	MapReduce(reduce, MAP, p, kolp);
	
	pthread_mutex_destroy(&mutex);
	for (int i=0; i<3; i++)
		cout << res[i] << " ";
	cout << "\n";
}

