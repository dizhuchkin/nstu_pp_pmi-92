#include <cstdlib>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

using namespace std;

#define err_exit(code, str)                        \
	{                                                \
		cerr << str << ": " << strerror(code) << endl; \
		exit(EXIT_FAILURE);                            \
	}

pthread_mutex_t mutex; // Mutex
int prod = 0;

void *thread_proizv(void *arg)
{
	int col_oper = 20;
	int err;
	while (true)
	{

		if (col_oper > 0)
		{
			err = pthread_mutex_lock(&mutex);
			if (err != 0)
				err_exit(err, "Cannot lock mutex");

			if (prod > 0)
			{
				err = pthread_mutex_unlock(&mutex);
				if (err != 0)
					err_exit(err, "Cannot unlock mutex");

				while (prod > 0)
				{
					sleep(1);
				}

				err = pthread_mutex_lock(&mutex);
				if (err != 0)
					err_exit(err, "Cannot lock mutex");
			}

			// Производим продукцию
			prod = rand() % 100;
			cout << "Произвели товара: " << prod << endl;
			err = pthread_mutex_unlock(&mutex);
			if (err != 0)
				err_exit(err, "Cannot unlock mutex");
			col_oper--;
		}
		else
			return (NULL);
	}
}

void *thread_prodav(void *arg)
{
	int col_oper = 20;
	int err;
	while (true)
	{
		if (col_oper > 0)
		{

			if (prod == 0)
			{
				err = pthread_mutex_unlock(&mutex);
				if (err != 0)
					err_exit(err, "Cannot unlock mutex");

				while (prod == 0)
				{
					sleep(1);
				}

				err = pthread_mutex_lock(&mutex);
				if (err != 0)
					err_exit(err, "Cannot lock mutex");
			}

			// Потребляем продукцию
			cout << "Потребили товар: " << prod << endl;
			prod = 0;
			sleep(1);
			err = pthread_mutex_unlock(&mutex);
			if (err != 0)
				err_exit(err, "Cannot unlock mutex");
			col_oper--;
		}
		else
			return (NULL);
	}
}

int main()
{
	pthread_t thread1, thread2;
	int err;
	int N;

	// Mutex
	err = pthread_mutex_init(&mutex, NULL);
	if (err != 0)
		err_exit(err, "Cannot initialize mutex");
	// Производит, если нет на складе
	err = pthread_create(&thread1, NULL, thread_proizv, NULL);
	if (err != 0)
		err_exit(err, "Cannot create thread1");

	// Продает, если есть товар
	err = pthread_create(&thread2, NULL, thread_prodav, NULL);
	if (err != 0)
		err_exit(err, "Cannot create thread2");

	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);

	cout << "Товара осталось на складе для продажи: " << prod << endl;
	pthread_mutex_destroy(&mutex);
}
