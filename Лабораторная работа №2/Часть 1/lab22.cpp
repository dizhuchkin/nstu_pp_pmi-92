#include <cstdlib>
#include <iostream>
#include <cstring>
#include <pthread.h>

using namespace std;

#define err_exit(code, str) { cerr << str << ": " << strerror(code) << endl; exit(EXIT_FAILURE); }

const int TASKS_COUNT = 10;

int task_list[TASKS_COUNT]; // Mas of exir
int current_task = 0; //Ukaz na tek zad
pthread_mutex_t mutex;//Mutex

void do_task(int task_no)
{
	/*Making task*/
	cout << "I'm working"<< endl;
}

void *thread_job(void *arg)
{
	int task_no;
	int err;
	//Chosing available task
	while(true){
		err = pthread_mutex_lock(&mutex);
		if (err!=0)
			err_exit(err, "Cannot lock mutex");
		task_no = current_task;
		current_task++;
		err = pthread_mutex_unlock(&mutex);
		if(err!=0)
			err_exit(err, "Cannot unlock mutex");
		if(task_no < TASKS_COUNT)
			do_task(task_no);
		else
			return NULL;
	}
	
}

int main()
{
	pthread_t thread1, thread2;
	int err;
	
	for (int i = 0; i<TASKS_COUNT; ++i)
		task_list[i] = rand()%TASKS_COUNT;
	
	err = pthread_mutex_init(&mutex, NULL);
	if (err!=0)
		err_exit(err, "Cannot initialize mutex");
	
	err = pthread_create(&thread1, NULL, thread_job, NULL);
	if(err!=0)
		err_exit(err, "Cannot create thread 1");
	
	err = pthread_create(&thread2, NULL, thread_job, NULL);
	if(err!=0)
		err_exit(err, "Cannot create thread 1");
	
	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);
	
	pthread_mutex_destroy(&mutex);
}
