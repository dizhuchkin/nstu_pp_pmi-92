#include <cstdlib>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <pthread.h>

using namespace std;

#define err_exit(code, str) { cerr << str << ": " << strerror(code) << endl; exit(EXIT_FAILURE); }

const int TASKS_COUNT = 10;

int task_list[TASKS_COUNT]; // Mas of exir
int kol_task_1[TASKS_COUNT] = {0,0,0,0,0,0,0,0,0,0};
int kol_task_2[TASKS_COUNT] = {0,0,0,0,0,0,0,0,0,0};
int current_task = 0; //Ukaz na tek zad
pthread_mutex_t mutex;//Mutex

void do_task(int task_no)
{
	int rez = 0;
	/*Making task*/
	
	while (task_no > 0){
		for(int i =0; i<100000;i++)
			rez = rez + task_no;
			
		task_no--;
		//do_task(task_no);
	}
}

void *thread_job(void *arg)
{
	int task_no;
	int err;
	int *num_thread = (int *) arg;//Number of thread
	//Chosing available task
	while(true){
		//err = pthread_mutex_lock(&mutex);
		//if (err!=0)
		//	err_exit(err, "Cannot lock mutex");
		task_no = current_task;
		sleep(1);
		current_task++;
		//err = pthread_mutex_unlock(&mutex);
		//if(err!=0)
		//	err_exit(err, "Cannot unlock mutex");
		if(task_no < TASKS_COUNT){
			do_task(task_no);
			if (*num_thread == 1)
				kol_task_1[task_no]++;
			else
				kol_task_2[task_no]++;
		}
		else
			return NULL;
	}
	
}

int main()
{
	pthread_t thread1, thread2;
	int err;
	int num_thread[2] = {1,2};//Number of thread
	
	for (int i = 0; i<TASKS_COUNT; ++i)
		task_list[i] = rand()%TASKS_COUNT;
	
	err = pthread_mutex_init(&mutex, NULL);
	if (err!=0)
		err_exit(err, "Cannot initialize mutex");
	err = pthread_create(&thread1, NULL, thread_job, (void *) &num_thread[0]);
	if(err!=0)
		err_exit(err, "Cannot create thread 1");
	err = pthread_create(&thread2, NULL, thread_job, (void *) &num_thread[1]);
	if(err!=0)
		err_exit(err, "Cannot create thread 1");
	
	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);
	
	pthread_mutex_destroy(&mutex);
	cout << "Выведем какой поток какую задачу взял на исполнение:" << endl;
	for (int i=0;i<TASKS_COUNT; i++)
		cout << "№" << i+1 << "   thread1 - " << kol_task_1[i] << "  thread2 - " << kol_task_2[i] << endl;
}
