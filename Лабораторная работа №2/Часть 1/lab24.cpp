#include <cstdlib>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <vector>

using namespace std;

#define err_exit(code, str)                        \
	{                                                \
		cerr << str << ": " << strerror(code) << endl; \
		exit(EXIT_FAILURE);                            \
	}

const int TASKS_COUNT = 100;

int task_list[TASKS_COUNT]; // Mas of exir
int current_task = 0;				// Ukaz na tek zad
pthread_mutex_t mutex;			// Mutex
pthread_spinlock_t spin;		// Spinlock

void do_task(int task_no)
{
	int rez = 0;

	while (task_no > 0)
	{
		for (int i = 0; i < 100; i++)
			rez = rez + task_no;

		task_no--;
	}
	/*Making task*/
	cout << "I'm working" << endl;
}

void *thread_job(void *arg)
{
	int task_no;
	int err;
	// Chosing available task
	while (true)
	{
		err = pthread_mutex_lock(&mutex);
		if (err != 0)
			err_exit(err, "Cannot lock mutex");

		/*
		err = pthread_spin_lock(&spin);
		if (err!=0)
			err_exit(err, "Cannot lock spinlock");*/

		task_no = current_task;
		current_task++;

		err = pthread_mutex_unlock(&mutex);
		if (err != 0)
			err_exit(err, "Cannot unlock mutex");

		/*
		err = pthread_spin_unlock(&spin);
		if(err!=0)
			err_exit(err, "Cannot unlock spinlock");*/

		if (task_no < TASKS_COUNT)
			do_task(task_no);
		else
			return NULL;
	}
}

int main()
{
	pthread_t thread;

	int err;
	int N;
	cout << "Введите количество потоков:";
	cin >> N;

	// Создаём индефикаторы потоков
	vector<pthread_t> threads(N);

	for (int i = 0; i < TASKS_COUNT; ++i)
		task_list[i] = rand() % TASKS_COUNT;

	// Mutex
	err = pthread_mutex_init(&mutex, NULL);
	if (err != 0)
		err_exit(err, "Cannot initialize mutex");

	/*
	//Spinlok
	err = pthread_spin_init(&spin, PTHREAD_PROCESS_PRIVATE);
	if (err!=0)
		err_exit(err, "Cannot initialize spinlock");*/

	clock_t start = clock();
	// Запускаем потоки
	for (int i = 0; i < threads.size(); i++)
	{
		err = pthread_create(&threads.at(i), NULL, thread_job, NULL);
		if (err != 0)
			err_exit(err, "Cannot create thread");
	}

	// ожидаем окончание всех потоков
	for (int i = 0; i < threads.size(); i++)
	{
		pthread_join(threads.at(i), NULL);
	}

	clock_t end = clock();

	long double seconds = (long double)(end - start) / CLOCKS_PER_SEC;
	cout << "Время работы программы:" << seconds << endl;

	pthread_mutex_destroy(&mutex);
	// pthread_spin_destroy(&spin);
}
