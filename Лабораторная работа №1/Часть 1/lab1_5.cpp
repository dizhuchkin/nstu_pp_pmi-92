#include <cstdlib>
#include <iostream>
#include <time.h>
#include <cstring>
#include <pthread.h>
#include <vector>

using namespace std;

void *thread_job(void *arg)//функция потока
{
	vector<int> *p = (vector<int> *) arg;
	vector<int> &param = *p;
	cout << "Thread is running... The quantity of parameters is "<< p->size() <<endl;
	cout << "The value of parameters:\n";
	for (int i=0; i < p->size(); i++){
		cout << i+1 << ") " << param[i] << endl;//вывод параметров
	}
	cout << "Thread end running...";
}

int main()
{
	pthread_t thread;
	pthread_attr_t thread_attr;
	int err;
	int par;
	cout << "Enter the number of parameters:";//ввод количество параметров
	cin >> par;
	vector<int>*param = new vector<int>(par);
	vector<int> &p = *param;
	cout << "Enter the value of parametrs:";//ввод параметров
	for (int i=0; i<par;i++){
		cin >> err;
		p[i] = err;
	}
	err = pthread_create(&thread, NULL,thread_job, (void *) param);//запуск потока
	
	if(err != 0){
		cout << "Cannot create a thread: " << strerror(err) << endl;
		exit(-1);
	}
	pthread_exit(NULL);
}

