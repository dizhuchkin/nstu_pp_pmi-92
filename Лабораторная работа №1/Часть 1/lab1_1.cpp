#include <cstdlib>
#include <iostream>
#include <time.h>
#include <cstring>
#include <pthread.h>

using namespace std;

void *thread_job(void *arg)//функция потока
{
	cout << "Thread is running..."<<endl;
}

int main()
{
	pthread_t thread;
	int err;
	srand(time(NULL));
	int n = rand()%100+1; //генерируем случайное число
	cout  << n << endl;
	while(n!=0){
		err = pthread_create(&thread, NULL,thread_job, NULL);//создаём новый поток
		if(err != 0){
			cout << "Cannot create a thread: " << strerror(err) << endl;
			exit(-1);
		}
		n--;
	}
	pthread_exit(NULL);
}

