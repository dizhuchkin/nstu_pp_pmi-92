#include <cstdlib>
#include <iostream>
#include <time.h>
#include <cstring>
#include <pthread.h>
#include <vector>

using namespace std;

void *thread_job(void *arg)
{
	vector<int> *p = (vector<int> *) arg;
	cout << "Thread end running... The quantity of parameters is "<< p->size() <<endl;
}

int main()
{
	pthread_t thread;
	pthread_attr_t thread_attr;
	int err;
	int par;
	cout << "Enter the number of parameters:";
	cin >> par;
	vector<int>*param = new vector<int>(par);
	vector<int> &p = *param;
	cout << "Enter the value of parametrs:";
	for (int i=0; i<par;i++){
		cin >> err;
		p[i] = err;
	}
	err = pthread_create(&thread, NULL,thread_job, (void *) param);
	
	if(err != 0){
		cout << "Cannot create a thread: " << strerror(err) << endl;
		exit(-1);
	}
	pthread_exit(NULL);
}

