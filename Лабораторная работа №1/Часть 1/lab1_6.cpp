#include <cstdlib>
#include <iostream>
#include <time.h>
#include <cstring>
#include <pthread.h>
#include <vector>
#include <utility>

using namespace std;

//Add 4
int func1(int d){
	return d+4;
}

//Mul 2
int func2(int d){
	return d*2;
}

//d^2
int func3(int d){
	return d*d;
}

void *thread_job(void *arg)//функция потока
{
	vector<pair<int,int>> *p = (vector<pair<int,int>> *)arg;
	vector<pair<int,int>> &mas = *p;
	int N = 0;

	cout << "Thread is running..."<<endl;

	switch(mas[mas.size()-1].first){//выполняем функцию, над элементами массива
		case(1):{
			while(N != mas.size()-1){
				if (mas[N].second == 0){
					mas[N].second = 1;
					mas[N].first = func1(mas[N].first);
					N++;
				}else{
					N++;
				}
			}
			break;
		}
		case(2):{
			while(N != mas.size()-1){
				if (mas[N].second == 0){
					mas[N].second = 1;
					mas[N].first = func2(mas[N].first);
					N++;
				}else{
					N++;
				}
			}			
			break;
		}
		case(3):{
			while(N != mas.size()-1){
				if (mas[N].second == 0){
					mas[N].second = 1;
					mas[N].first = func3(mas[N].first);
					N++;
				}else{
					N++;
				}
			}			
			break;
		}
		default:{
			cout << "Error!!! Wrong number of function";
			exit(-1);
			break;
		}
	}
}

int main()
{
	pthread_t thread;
	int err;
	vector<pair<int,int>> *m = new vector<pair<int,int>>();
	vector<pair<int,int>> &p = *m;

	

	int numf, kolp,sizemas;//вводим параметры
	cout << "Enter the number of function:";
	cin >> numf;
	cout << "Enter size of mass:";
	cin >> sizemas;
	cout << "Enter the value of threads:";
	cin >> kolp;

	vector<pthread_t> threads(kolp);//массив потоков
	
	//Заполняем массив случайными числами 1 до 20
	srand(time(NULL));
	cout << "Start:\n";
	for (int i=0; i < sizemas; i++){
		p.push_back(make_pair(rand()%20+1,0));
		cout << p[i].first << " ";
	}
		
	p.push_back(make_pair(numf,0));
	
	cout << "\n";
	//Запускаем потоки
	for (int i=0; i < kolp; i++){
		//создаём потока
		err = pthread_create(&threads.at(i), NULL,thread_job, (void *) m);
	
		if(err != 0){
			cout << "Cannot create a thread: " << strerror(err) << endl;
			exit(-1);
		}

	}

	int j = 0;

	while(j < kolp){//ожидаем завершение всех потоков
		pthread_join(threads.at(j), NULL);
		j = j + 1;
	}

	
	cout << "Rezult:\n";//выводим результат
	for (int i=0; i < sizemas; i++){
		cout << p[i].first << " ";
	}

	cout << "\n";
}

