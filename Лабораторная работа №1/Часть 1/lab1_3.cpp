#include <cstdlib>
#include <iostream>
#include <time.h>
#include <cstring>
#include <pthread.h>

using namespace std;

void *thread_job(void *arg)//функция потока
{
	cout << "Thread end running..."<<endl;
}

int main()
{
	pthread_t thread;
	pthread_attr_t thread_attr;
	int err;
	
	err = pthread_attr_init(&thread_attr);//инициализация атрибутов потока
	if(err != 0){
		cout << "Cannot create thread attribute: " << strerror(err) << endl;
		exit(-1);
	}
	
	int k=1, n;
	//Производим настройку атрибутов потока
	while(k){
		cout << "Enter a number of action:" << endl; 
		cout << "1)Change the type of thread\n";
		cout << "2)Change the guard size\n";
		cout << "3)Change the stack addres\n";
		cout << "4)Change the stack size\n";
		cout << "5)End changing\n";
		cin >> n;
		
		switch(n){
			case(1):{//Выбор типа потока
				cout << "Enter the type of thread:\n";
				cout << "1)Detached\n";
				cout << "2)Joinable\n";
				cout << "3)Leave by default\n";
				cin >> n;
				if (n == 1){
					err = pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_DETACHED);
					if (err != 0){
						cout << "Setting thread type failed:"<< strerror(err)<<endl;
					}
				} else if (n == 2){
					err = pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_JOINABLE);
					if (err != 0){
						cout << "Setting thread type failed:"<< strerror(err)<<endl;
					}					
				} else if (n != 3) {
					cout << "Wrong number of action!!!\n";	
				}
				break;
			}
			case(2):{//Ввод размера защиты
				cout << "Enter size in bytes:";
				size_t size;
				cin >> size;
				err = pthread_attr_setguardsize(&thread_attr,size);
				if (err != 0){
						cout << "Setting guard size failed:"<< strerror(err)<<endl;
					}
				break;
			}
			case(3):{//Ввод адреса стека
				cout << "Enter stacks adress:";
				int size;
				size_t stacksize;
				err = pthread_attr_getstacksize(&thread_attr,&stacksize);
				if(err != 0) {
					cout << "Can't get stack size:" << strerror(err) << endl;
				}
				cin >> size;
				err = pthread_attr_setstack(&thread_attr,(void *) &size,stacksize);
				if (err != 0){
						cout << "Setting stack addr failed:"<< strerror(err)<<endl;
					}
				break;
			}
			case(4):{//Ввод размера стека
				cout << "Enter stacks' size (MB):";
				size_t size;
				cin >> size;
				err = pthread_attr_setstacksize(&thread_attr,size*1024*1024);
				if (err != 0){
						cout << "Setting stack addr failed:"<< strerror(err)<<endl;
					}
				break;
			}
			case(5):{
				k = 0;
				break;
			}
			default:{
				cout << "\nWrong number of action!\n\n";
				break;
			}
		}
	}
			
	err = pthread_create(&thread, &thread_attr,thread_job, NULL);//создание потока
	
	if(err != 0){
		cout << "Cannot create a thread: " << strerror(err) << endl;
		exit(-1);
	}
	pthread_attr_destroy(&thread_attr);
	pthread_exit(NULL);
}

