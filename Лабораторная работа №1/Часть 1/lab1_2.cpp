#include <cstdlib>
#include <iostream>
#include <time.h>
#include <cstring>
#include <iomanip>
#include <pthread.h>

using namespace std;

void *thread_job(void *arg)//функция потока
{
	cout << "Thread start running..."<<endl;
	int *param = (int *) arg;
	int c, a = 2, b = 7;
	
	clock_t start = clock();
	for (int i = 0; i < *param; i++){
		c = a * b;
	}
	clock_t end = clock();
	
	cout << "Thread end running..."<<endl;
	long double seconds = (long double)(end-start)/CLOCKS_PER_SEC;//время работы операции в потоке
	cout << "Время операций в потокe:" << seconds << endl;
}

int main()
{
	pthread_t thread;
	int err;
	int a = 2,b = 7,c;
	int param;//Количество операций, задаем с консоли
	cout << "Enter the number of operations:";
	cin >> param;
	clock_t start = clock();
	for(int i = 0; i < param; i++){
		c = a * b;
	}
	clock_t end = clock();
	long double seconds = (long double)(end-start)/CLOCKS_PER_SEC;//время работы вне потока
	cout << "Время операций вне потока:" << fixed << setprecision(10) <<seconds << endl;
	
	
	err = pthread_create(&thread, NULL,thread_job, (void *)&param);//Передаем количество операций в поток
	if(err != 0){
		cout << "Cannot create a thread: " << strerror(err) << endl;
		exit(-1);
	}
	pthread_exit(NULL);
}

