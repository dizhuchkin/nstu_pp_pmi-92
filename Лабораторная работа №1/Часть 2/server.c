#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <err.h>
#include <pthread.h>

//структура данных для потока
typedef struct someArgs_tag {
    int client_fd; //индефикатор клиента
    int numberRequest;//номер запроса
    pthread_t thread;//индефикатор потока
} argsThread;

//Потоковая функция
typedef void* (*thread_function)(void*);

//подсчёт цифр в числе
int findCount(int n)
{
    int count = 0;
    while (n != 0) {
        count++;
        n /= 10;
    }
    return count;
}

//страница с счётчиком запросов
char* page1 = "HTTP/1.1 200 OK\r\n"
"Content-Type: text/html; charset=UTF-8\r\n"
"<!DOCTYPE html>\r\n\r\n<html>\r\n\t<head>\r\n\t\t<title>Сервер</title>\r\n\t</head>\r\n"
"\t<body>\r\n\t\t<h1>Request number %i has been processed</h1>\r\n"
"\t</body>\r\n</html>";

//страница с счётчиком запросов и версией php
char* page2 = "HTTP/1.1 200 OK\r\n"
"Content-Type: text/html; charset=UTF-8\r\n"
"<!DOCTYPE html>\r\n\r\n<html>\r\n\t<head>\r\n\t\t<title>Сервер</title>\r\n\t</head>\r\n"
"\t<body>\r\n\t\t<h1>Request number %i has been processed</h1>"
"\r\n\t\t<div>PHP version: %s</div>\r\n\t</body>\r\n</html>";

//функция, которая будет выполняться в потоке (отправляет страницу page1)
void *thread_func_1(void *args){

  argsThread *arg = (argsThread*) args;

  size_t response_size = strlen(page1) + findCount(arg->numberRequest);
  char* response = (char*)malloc(sizeof(char) * response_size);

  //создаём ответ клиенту
  snprintf(response, response_size, page1, arg->numberRequest);

  recv(arg->client_fd, NULL, 1, 0);

  //отправка результата клиенту
  if (write(arg->client_fd, response, response_size) != response_size){
    perror("Write");
  }
  
  //закрываем соединение
  close(arg->client_fd);
  arg->client_fd = -1;
  //завершение работы потока
  pthread_exit(NULL);
}

//функция, которая будет выполняться в потоке (отправляет страницу page2)
void *thread_func_2(void *args){

  argsThread *arg = (argsThread*) args;

  //запускаем php-скрипт
  FILE* fp = popen("php -r \"echo phpversion();\"", "r");

  char php_script[20];

  if (fscanf(fp, "%s", php_script) == 1) {
    pclose(fp);

    size_t response_size = strlen(page2) + findCount(arg->numberRequest) + 20;
    char* response = (char*)malloc(sizeof(char) * response_size);

    recv(arg->client_fd, NULL, 1, 0);

    //создаём ответ клиенту
    snprintf(response, response_size, page2, arg->numberRequest, php_script);

    //отправляем результат клиенту
    if (write(arg->client_fd, response, response_size) != response_size){
      perror("Write");
    }
  }

  //закрываем соединение 
  shutdown(arg->client_fd, SHUT_WR);
  close(arg->client_fd);
  arg->client_fd = -1;
  //завершение работы потока
  pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
  int one = 1, result = 0, i = 0, sizeStack = 0, numberC = 1;
  char* resp;
  thread_function thread_func;

  //параметры потоковой функции
  argsThread args[150];
  //аргументы потоковой функции
  pthread_attr_t thread1_attr;

  for (i = 0; i < 100; i++){
    args[i].client_fd = -1;
    args[i].numberRequest = -1;
  }

  struct sockaddr_in svr_addr, cli_addr;
  socklen_t sin_len = sizeof(cli_addr);

  if (argc != 3){ 
    printf("Parameters entered incorrectly\n");
    return -1;
  }

  //параметры сервера
  int arg1 = atoi(argv[1]); 
  int arg2 = atoi(argv[2]); 

  switch(arg1)//выбор размер стека
  {
    case 1: 
      sizeStack = 2*1024*1024;
      break;
    case 2: 
      sizeStack = 1*1024*1024;
      break;
    case 3: 
      sizeStack = 512*1024;
      break;
    default: 
      sizeStack = 1*1024*1024;
      break;
  }

  switch(arg2)//выбор поточной функции
  {
    case 1: 
      resp = page1;
      thread_func = thread_func_1;
      break;
    case 2: 
      resp = page2;
      thread_func = thread_func_2;
      break;
    default: 
      resp = page1;
      thread_func = thread_func_1;
      break;
  }
  //создание сокета
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0){
    perror("Socket");
    return -1;
  }

  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int));

  int port = 8080;
  svr_addr.sin_family = AF_INET;
  svr_addr.sin_addr.s_addr = INADDR_ANY;
  svr_addr.sin_port = htons(port);

  //привязка сокета к порту
  if (bind(sock, (struct sockaddr *) &svr_addr, sizeof(svr_addr)) == -1) {
    close(sock);
    perror("Bind");
    return -1;
  }

  result = listen(sock, 50);//прослушивание порта
  if (result != 0){
    perror("Listen");
    return -1;
  }

  printf("Server Configuration:\n");
  printf("Stack Size: %f MB\n", (double)sizeStack/(1024*1024));
  printf("Server response: \n\n%s\n\n", resp);
  printf("Server listening on http://127.0.0.1:8080\n");

  //инициализируем атрибуты потока
  result = pthread_attr_init(&thread1_attr);
  if (result != 0){
    perror("Pthread_attr_init");
    return -1;
  }

  //Размер стека
  result = pthread_attr_setstacksize(&thread1_attr, sizeStack);
  if (result != 0){
    perror("Pthread_attr_setstacksize");
    return -1;
  }

  //Тип потока
  result = pthread_attr_setdetachstate(&thread1_attr, PTHREAD_CREATE_DETACHED);
  if (result != 0){
    perror("Pthread_attr_setdetachstate");
    return -1;
  }

  i = 0;

  //обработка запросов
  while (1) {

    if (args[i].client_fd != -1){
      continue;
    }

    args[i].client_fd = accept(sock, (struct sockaddr *) &cli_addr, &sin_len);

    if (args[i].client_fd == -1) {
      perror("Accept");
      continue;
    } 

    printf("Connection %d\n", numberC);
    args[i].numberRequest = numberC;

    //создание нового потока
    result = pthread_create(&args[i].thread,  &thread1_attr, thread_func, (void*) &args[i]);
    if (result != 0){
      perror("Create thread");
    }

    i = i + 1;
    numberC = numberC + 1;

    if (i >= 80) {
      i = 0;
    }
  }

  pthread_attr_destroy(&thread1_attr);
  close(sock);
  return 0;
}